#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
import time

def list_maker():
    lista = []
    for i in range(25):
        lista.insert(i, random.randrange(1, 17))

    print("lista inicial desordenada:")
    print(lista)
    return lista

def ordenar(lista):

    for i in range(1,len(lista)):
        for j in range(0, len(lista) - 1):
            if (lista[j + 1] < lista [j]):
                temp = lista[j];
                lista[j] = lista[j + 1];
                lista[j + 1] = temp
    return lista

if __name__ == '__main__':

    lista = list_maker()
    lista2 = ordenar(lista)



    print("lista ordenada:")
    print(lista2)
