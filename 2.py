#!/usr/bin/env python
# -*- coding: utf-8 -*-

def matrix_maker():
    matrix = []

    for i in range(N):
        # Añadir listas para generar cada fila.
        matrix.append([])
        for j in range(altura):
            # Añadir sublistas/columnas en cada fila.
            if ((j > i) and (j < (altura - i - 1))):
                matrix[i].append(' ')
            else:
                matrix[i].append('*')
    return matrix

def impresora(matriz, N, altura):

    for i in range(N):
        # Variable se actualiza en cada fila.
        casilla = ''
        print('\n')
        for j in range(altura):
            # Acumulación de las columnas de cada fila.
            casilla += matriz[i][j] + ' '
        print(casilla)
    print('\n')# Variable se actualiza en cada fila.

if __name__ == '__main__':

    matriz_triangular = []

    N = int(input("ingresa un numero para la matriz a utilizar: "))
    altura = N * 2

    matriz = matrix_maker()
    impresora(matriz, N, altura)
