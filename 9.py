#!/usr/bin/env python
# -*- coding: utf-8 -*-


def matrix_maker():
    matrix = []

    for i in range(N):
        # Añadir listas para generar cada fila.
        matrix.append([])
        for j in range(N):
            # Añadir sublistas/columnas en cada fila.
            if (i == j):
                matrix[i].append('1')
            else:
                matrix[i].append('0')
    return matrix

def impresora(matriz, N):

    for i in range(N):
        # Variable se actualiza en cada fila.
        casilla = ''
        for j in range(N):
            # Acumulación de las columnas de cada fila.
            casilla += matriz[i][j] + ' '
        print(casilla)
    print('\n')# Variable se actualiza en cada fila.


if __name__ == '__main__':

    matriz = []
    N = int(input("ingresa un numero para la matriz a utilizar: "))
    matriz = matrix_maker()
    impresora(matriz, N)
