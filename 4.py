#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
import time

def list_maker():
    lista = []
    for i in range(25):
        lista.insert(i, random.randrange(1, 17))
    print(lista)
    return lista

def requisitos(lista_inicial, N):
    lista1 = []
    lista2 = []
    lista3 = []
    lista4 = []

    for i in lista_inicial:
        if i > N:
            lista1.append(i)
        if i < N:
            lista2.append(i)
        if  i == N:
            lista3.append(i)
        if i%N == 0:
            lista4.append(i)
    print("números mayores:")
    print(sorted(lista1))
    print("números menores:")
    print(sorted(lista2))
    print("números iguales:")
    print(sorted(lista3))
    print("números múltiplos:")
    print(sorted(lista4))
    return lista1, lista2, lista3, lista4

if __name__ == '__main__':

    lista_inicial = list_maker()
    N = int(input("ingresa un numero para utilizar: "))
    requisitos(lista_inicial, N)
