#!/usr/bin/env python
# -*- coding: utf-8 -*-
def decodificador(frase1):

    temporal = frase1.replace("X", "")
    frase2 = temporal[:: -1]

    return frase2

if __name__ == '__main__':

    frase2 = ""
    print("frase codificada:")
    print("¡XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt")
    frase1 = "¡XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt"
    print("decodificada:")
    frase2 = decodificador(frase1)
    print(frase2)
